package com.example.demo.service.impl;


import com.example.demo.service.DemoService;
import com.reger.remoting.annotation.Export;

@Export
public class DemoServiceImpl implements DemoService {

	@Override
	public int add(int a, int b) {
		return a+b;
	}

	@Override
	public String add() {
		return "Hello Word !";
	}

	@Override
	public String test(String tts) {
		return tts;
	}

}
