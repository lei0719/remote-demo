package com.example.demo.service;

import com.reger.remoting.annotation.ServerId;

@ServerId("server")
public interface DemoService {
	int add(int a,int b);

	String add();
	
	String test(String tts);
}
