package com.example.demo.service.impl;


import com.example.demo.service.DemoService2;
import com.reger.remoting.annotation.Export;

@Export
public class DemoServiceImpl2 implements DemoService2 {

	@Override
	public int add(int a, int b) {
 		return a+b;
	}

}
