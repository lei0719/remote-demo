package com.example.demo;

import java.util.UUID;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.reger.remoting.filter.ConsumerFilter;

@SpringBootApplication()
public class DemoApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	public void run(String... args) throws Exception {
	}


	public @Bean ConsumerFilter consumerFilter() {
		return r->{
			r.addHeader("XID", UUID.randomUUID().toString());
			return r.request();	
		};
	}
}
