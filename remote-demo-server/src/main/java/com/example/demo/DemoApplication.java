package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

import com.reger.remoting.filter.ProviderFilter;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
//	@ConditionalOnBean
//	public@Bean MultipartResolver multipartResolver() {
//		return new StandardServletMultipartResolver();
//	}
	@Autowired MultipartResolver multipartResolver;
	@Override
	public void run(String... args) throws Exception {
	}

	public @Bean ProviderFilter providerFilter() {
		return r->{
			System.err.println(r.getHeaders());
			return r.request();	
		};
	}
}
