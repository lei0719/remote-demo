package com.example.demo;

import java.net.SocketException;

import org.springframework.boot.CommandLineRunner;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.DemoService;
import com.example.demo.service.DemoService2;
import com.reger.remoting.annotation.Inject;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class DemoClientController implements CommandLineRunner {


	public void run(String... args) throws Exception {
		long s=System.currentTimeMillis();
		for (int i = 0; i < 100000; i++) {
			demoService1.add(i, 1);
			if(i%10000==0) {
				long e=System.currentTimeMillis();
				log.info("执行时间{}秒,执行{}次",(e-s)/1000.0,i);
				s=e;
			}
		}
		 s=System.currentTimeMillis();
		for (int i = 0; i < 1000000; i++) {
			demoService4.add(i, 1);
			if(i%10000==0) {
				long e=System.currentTimeMillis();
				log.info("执行时间{}秒,执行{}次",(e-s)/1000.0,i);
				s=e;
			}
		}
	}
	@Inject(protocol = "http",serializable = "kryo")
	DemoService demoService1;
	@Inject(serviceId = "server",protocol = "websocket",serializable = "kryo")
	DemoService2 demoService4;

	@PostMapping("test")
	public Object test(String tts) {
		return demoService1.test(tts);
	}
	@GetMapping("addwebsocket")
	public Object add0(Integer b, Integer a) {
		return demoService1.add();
	}
	@GetMapping("addhttp")
	public Object add1(Integer b, Integer a) {
		return demoService4.add(a, b);
	}

	@GetMapping("add2")
	public Object add2(Integer b, Integer a) throws SocketException {
		return a + b;
	}

}
